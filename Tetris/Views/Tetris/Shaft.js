var backgroundColor = "#FFFFFF";
var elementSize = 25;
var pieces;
var Piece = (function () {
    function Piece(config, color, id) {
        this.color = color;
        this.id = id;
        this.grid = [];
        for (var i = 0; i < 4; i++) {
            this.grid.push([]);
            var c = i < config.length ? config[i] : 0;
            for (var j = 0; j < 4; j++) {
                this.grid[i].push((c & 1) == 1);
                c >>= 1;
            }
        }
    }
    Piece.prototype.cloneGrid = function () {
        return _.cloneDeep(this.grid);
    };
    Piece.prototype.getColor = function () {
        return this.color;
    };
    Piece.prototype.getId = function () {
        return this.id;
    };
    return Piece;
}());
var PieceInstance = (function () {
    function PieceInstance(piece) {
        this.piece = piece;
        this.grid = piece.cloneGrid();
    }
    PieceInstance.prototype.rotate = function (count) {
        for (var c = 0; c < count; c++) {
            var grid2_1 = [];
            for (var i = 0; i < this.grid.length; i++) {
                grid2_1[i] = [];
                for (var j = 0; j < this.grid[i].length; j++)
                    grid2_1[i][j] = this.grid[j][3 - i];
            }
            this.grid = grid2_1;
        }
        var emptyRows = -1;
        var empty = true;
        while (empty) {
            emptyRows++;
            for (var i = 0; i < this.grid[emptyRows].length; i++)
                if (this.grid[emptyRows][i]) {
                    empty = false;
                    break;
                }
        }
        var emptyColumns = -1;
        empty = true;
        while (empty) {
            emptyColumns++;
            for (var i = 0; i < this.grid.length; i++)
                if (this.grid[i][emptyColumns]) {
                    empty = false;
                    break;
                }
        }
        var grid2 = _.cloneDeep(this.grid);
        for (var i = 0; i < this.grid.length; i++)
            for (var j = 0; j < this.grid[i].length; j++)
                this.grid[i][j] = false;
        for (var i = emptyRows; i < this.grid.length; i++)
            for (var j = emptyColumns; j < this.grid[i].length; j++)
                this.grid[i - emptyRows][j - emptyColumns] = grid2[i][j];
    };
    PieceInstance.prototype.getHeight = function () {
        var result = 0;
        for (var i = 0; i < this.grid.length; i++) {
            for (var j = 0; j < this.grid[i].length; j++) {
                if (this.grid[i][j]) {
                    result++;
                    break;
                }
            }
        }
        return result;
    };
    PieceInstance.prototype.setPosition = function (y, x) {
        this.y = y;
        this.x = x;
    };
    PieceInstance.prototype.move = function (y, x) {
        this.y += y;
        this.x += x;
    };
    PieceInstance.prototype.draw = function (context) {
        for (var i = 0; i < this.grid.length; i++) {
            for (var j = 0; j < this.grid[i].length; j++) {
                if (this.grid[i][j] && this.y + i >= 0) {
                    context.fillStyle = this.piece.getColor();
                    context.fillRect((this.x + j) * elementSize + 1, (this.y + i) * elementSize + 1, elementSize - 2, elementSize - 2);
                }
            }
        }
    };
    PieceInstance.prototype.clear = function (context) {
        for (var i = 0; i < this.grid.length; i++) {
            for (var j = 0; j < this.grid[i].length; j++) {
                if (this.grid[i][j] && this.y + i >= 0) {
                    context.fillStyle = backgroundColor;
                    context.fillRect((this.x + j) * elementSize, (this.y + i) * elementSize, elementSize, elementSize);
                }
            }
        }
    };
    PieceInstance.prototype.getGrid = function () {
        return this.grid;
    };
    PieceInstance.prototype.getX = function () {
        return this.x;
    };
    PieceInstance.prototype.getY = function () {
        return this.y;
    };
    PieceInstance.prototype.getId = function () {
        return this.piece.getId();
    };
    PieceInstance.prototype.clone = function () {
        var result = new PieceInstance(this.piece);
        result.grid = _.cloneDeep(this.grid);
        result.x = this.x;
        result.y = this.y;
        return result;
    };
    return PieceInstance;
}());
var Shaft = (function () {
    function Shaft(height, width, canvasContext) {
        this.height = height;
        this.width = width;
        this.canvasContext = canvasContext;
        this.content = [];
        for (var i = 0; i < height; i++) {
            this.content[i] = [];
            for (var j = 0; j < width; j++)
                this.content[i].push(0);
        }
    }
    Shaft.prototype.isEmpty = function (y, x) {
        return y < 0 || y < this.height && x >= 0 && x < this.width && !this.content[y][x];
    };
    Shaft.prototype.canMove = function (piece, y, x) {
        var grid = piece.getGrid();
        for (var i = 0; i < grid.length; i++) {
            for (var j = 0; j < grid[i].length; j++) {
                if (grid[i][j] && !this.isEmpty(y + i, x + j))
                    return false;
            }
        }
        return true;
    };
    Shaft.prototype.move = function (y, x) {
        var canMove = this.canMove(this.currentPiece, this.currentPiece.getY() + y, this.currentPiece.getX() + x);
        if (canMove) {
            this.currentPiece.clear(this.canvasContext);
            this.currentPiece.move(y, x);
            this.currentPiece.draw(this.canvasContext);
        }
        return canMove;
    };
    Shaft.prototype.rotate = function (count) {
        var clone = this.currentPiece.clone();
        clone.rotate(count);
        var canMove = this.canMove(clone, this.currentPiece.getY(), this.currentPiece.getX());
        if (canMove) {
            this.currentPiece.clear(this.canvasContext);
            this.currentPiece.rotate(count);
            this.currentPiece.draw(this.canvasContext);
        }
        return canMove;
    };
    Shaft.prototype.finishPiece = function () {
        var grid = this.currentPiece.getGrid();
        for (var i = 0; i < grid.length; i++) {
            for (var j = 0; j < grid[i].length; j++) {
                if (grid[i][j])
                    this.content[this.currentPiece.getY() + i][this.currentPiece.getX() + j] = this.currentPiece.getId() + 1;
            }
        }
    };
    Shaft.prototype.randomNumber = function (limit) {
        return Math.floor(Math.random() * limit);
    };
    Shaft.prototype.generateNextPiece = function () {
        this.currentPiece = new PieceInstance(pieces[this.randomNumber(pieces.length)]);
        var rotation = this.randomNumber(4);
        this.currentPiece.rotate(rotation);
        this.currentPiece.setPosition(-this.currentPiece.getHeight() + 1, (this.width >> 1) - 1);
        this.currentPiece.draw(this.canvasContext);
        return rotation;
    };
    Shaft.prototype.setNextPiece = function (index, rotation, y, x) {
        this.currentPiece = new PieceInstance(pieces[index]);
        this.currentPiece.rotate(rotation);
        this.currentPiece.setPosition(y, x);
        this.currentPiece.draw(this.canvasContext);
    };
    Shaft.prototype.removeFullRows = function () {
        var rowsRemoved = 0;
        for (var i = this.height - 1; i >= 0; i--) {
            var isFull = true;
            for (var j = 0; j < this.width; j++) {
                if (!this.content[i][j]) {
                    isFull = false;
                }
            }
            if (isFull) {
                for (var j = i; j > 0; j--) {
                    for (var k = 0; k < this.width; k++) {
                        this.content[j][k] = this.content[j - 1][k];
                    }
                }
                i++;
                rowsRemoved++;
            }
        }
        if (rowsRemoved > 0) {
            for (var i = 0; i < this.height; i++) {
                for (var j = 0; j < this.width; j++) {
                    this.canvasContext.fillStyle = backgroundColor;
                    this.canvasContext.fillRect(j * elementSize, i * elementSize, elementSize, elementSize);
                    this.canvasContext.fillStyle = this.content[i][j] ? pieces[this.content[i][j] - 1].getColor() : backgroundColor;
                    this.canvasContext.fillRect(j * elementSize + 1, i * elementSize + 1, elementSize - 2, elementSize - 2);
                }
            }
        }
        return rowsRemoved;
    };
    Shaft.prototype.getWidth = function () {
        return this.width;
    };
    Shaft.prototype.getCurrentPiece = function () {
        return this.currentPiece;
    };
    return Shaft;
}());
var Keys;
(function (Keys) {
    Keys[Keys["Up"] = 0] = "Up";
    Keys[Keys["Down"] = 1] = "Down";
    Keys[Keys["Left"] = 2] = "Left";
    Keys[Keys["Right"] = 3] = "Right";
})(Keys || (Keys = {}));
;
var Game = (function () {
    function Game(canvasContext, canvasContextOther, audioParentElement) {
        this.canvasContext = canvasContext;
        this.canvasContextOther = canvasContextOther;
        this.audioParentElement = audioParentElement;
    }
    Game.prototype.playBounce = function () {
        this.bounceElement.play();
    };
    Game.prototype.handleKey = function (code, down) {
        var handled = false;
        var index = -1;
        switch (code) {
            case 37:
                index = Keys.Left;
                break;
            case 39:
                index = Keys.Right;
                break;
            case 38:
                index = Keys.Up;
                break;
            case 40:
                index = Keys.Down;
                break;
        }
        if (index >= 0) {
            if (down) {
                if (!this.keyResetNeeded[index]) {
                    this.keyState[index] = true;
                }
            }
            else {
                this.keyResetNeeded[index] = false;
                this.keyState[index] = false;
            }
        }
        return index >= 0;
    };
    Game.prototype.initKeyboard = function () {
        var _this = this;
        this.keyState = [];
        this.keyResetNeeded = [];
        for (var i = 0; i < 4; i++) {
            this.keyState.push(false);
            this.keyResetNeeded.push(false);
        }
        window.addEventListener("keydown", function (e) {
            if (_this.handleKey(e.keyCode, true)) {
                e.preventDefault();
            }
        });
        window.addEventListener("keyup", function (e) {
            if (_this.handleKey(e.keyCode, false)) {
                e.preventDefault();
            }
        });
    };
    Game.prototype.initPieces = function () {
        pieces = [];
        pieces.push(new Piece([15], "#00FFFF", 0));
        pieces.push(new Piece([7, 4], "#0000FF", 1));
        pieces.push(new Piece([7, 1], "#FF8000", 2));
        pieces.push(new Piece([3, 3], "#DDAA00", 3));
        pieces.push(new Piece([6, 3], "#00FF00", 4));
        pieces.push(new Piece([7, 2], "#FF00FF", 5));
        pieces.push(new Piece([3, 6], "#FF0000", 6));
    };
    Game.prototype.initPlayArea = function () {
        this.speed = 30;
        this.speedCounter = 0;
        this.shaft = new Shaft(22, 11, this.canvasContext);
        this.shaftOther = new Shaft(22, 11, this.canvasContextOther);
    };
    Game.prototype.startTimer = function () {
        var _this = this;
        setInterval(function () { return _this.dropPiece(); }, 20);
    };
    Game.prototype.dropPiece = function () {
        if (this.keyState[Keys.Left]) {
            if (this.shaft.move(0, -1)) {
                this.playBounce();
                this.tetrisSocketHub.move(-1);
            }
            this.keyState[Keys.Left] = false;
            this.keyResetNeeded[Keys.Left] = true;
        }
        if (this.keyState[Keys.Right]) {
            if (this.shaft.move(0, 1)) {
                this.playBounce();
                this.tetrisSocketHub.move(1);
            }
            this.keyState[Keys.Right] = false;
            this.keyResetNeeded[Keys.Right] = true;
        }
        if (this.keyState[Keys.Up]) {
            if (this.shaft.rotate(1)) {
                this.playBounce();
                this.tetrisSocketHub.rotate();
            }
            this.keyState[Keys.Up] = false;
            this.keyResetNeeded[Keys.Up] = true;
        }
        if (this.keyState[Keys.Down]) {
            if (this.shaft.move(1, 0)) {
                this.playBounce();
                this.tetrisSocketHub.down();
            }
            else {
                this.speedCounter = this.speed;
            }
        }
        if (++this.speedCounter >= this.speed) {
            this.speedCounter = 0;
            if (this.shaft.move(1, 0)) {
                this.tetrisSocketHub.down();
            }
            else {
                this.keyState[Keys.Down] = false;
                this.keyResetNeeded[Keys.Down] = true;
                this.shaft.finishPiece();
                var rowsRemoved = this.shaft.removeFullRows();
                this.speed -= rowsRemoved;
                if (rowsRemoved > 0) {
                    this.bombElement.play();
                    this.katusaElement.playbackRate = this.katusaElement.playbackRate + 0.05;
                }
                this.tetrisSocketHub.dropped();
                var rotation = this.shaft.generateNextPiece();
                this.tetrisSocketHub.newPiece(this.shaft.getCurrentPiece().getId(), rotation, this.shaft.getCurrentPiece().getY(), this.shaft.getCurrentPiece().getX());
            }
        }
    };
    Game.prototype.initAudio = function () {
        this.bombElement = this.audioParentElement.querySelector("#bomb");
        this.bounceElement = this.audioParentElement.querySelector("#bounce");
        this.katusaElement = this.audioParentElement.querySelector("#katusa");
        this.katusaElement.volume = 0.2;
        this.katusaElement.playbackRate = 0.5;
        this.katusaElement.play();
    };
    Game.prototype.newPieceOther = function (index, rotation, y, x) {
        this.shaftOther.setNextPiece(index, rotation, y, x);
    };
    Game.prototype.moveOther = function (offset) {
        this.shaftOther.move(0, offset);
    };
    Game.prototype.rotateOther = function () {
        this.shaftOther.rotate(1);
    };
    Game.prototype.downOther = function () {
        this.shaftOther.move(1, 0);
    };
    Game.prototype.droppedOther = function () {
        this.shaftOther.finishPiece();
        this.shaftOther.removeFullRows();
    };
    Game.prototype.startOther = function () {
        // Connection initialized
        this.initAudio();
        this.initKeyboard();
        this.initPieces();
        this.initPlayArea();
        var rotation = this.shaft.generateNextPiece();
        this.tetrisSocketHub.newPiece(this.shaft.getCurrentPiece().getId(), rotation, this.shaft.getCurrentPiece().getY(), this.shaft.getCurrentPiece().getX());
        this.startTimer();
    };
    Game.prototype.connectSocket = function () {
        var _this = this;
        this.tetrisSocketHub = $.connection.tetrisSocketHub.server;
        var tetrisClient = $.connection.tetrisSocketHub.client;
        tetrisClient.newPiece = function (index, rotation, y, x) { return _this.newPieceOther(index, rotation, y, x); };
        tetrisClient.move = function (offset) { return _this.moveOther(offset); };
        tetrisClient.rotate = function () { return _this.rotateOther(); };
        tetrisClient.down = function () { return _this.downOther(); };
        tetrisClient.dropped = function () { return _this.droppedOther(); };
        tetrisClient.start = function () { return _this.startOther(); };
        $.connection.hub.start().done(function () {
        });
    };
    Game.prototype.connect = function () {
        this.connectSocket();
    };
    Game.prototype.start = function () {
        this.tetrisSocketHub.start();
    };
    return Game;
}());
//# sourceMappingURL=Shaft.js.map